function make_paddle(x, y, length)
    local paddle = {}
    paddle["y"] = y
    paddle["x"] = x
    paddle["length"] = length

    return paddle
end 

function draw_paddle(paddle)
    love.graphics.rectangle( "fill", 
        paddle["x"], 
        paddle["y"], 
        PADDLE_WIDTH, 
        paddle["length"], 
        0, 0, 
        10 )
end

function love.load()
    SCREEN_WIDTH = 800
    SCREEN_HEIGHT = 600
    PADDLE_LENGTH = 250
    PADDLE_WIDTH = 10

    PADDLE_OFFSET = 30
    

    love.window.setMode(SCREEN_WIDTH, SCREEN_HEIGHT, {resizable=false})

    time_elapsed = 0
    second_count = 0

    circle = {}
    circle["x"] = SCREEN_WIDTH / 2
    circle["y"] = SCREEN_HEIGHT / 2
    
    player_paddle = make_paddle(PADDLE_OFFSET, (SCREEN_HEIGHT - PADDLE_LENGTH)/ 2, PADDLE_LENGTH)
    opponent_paddle = make_paddle(SCREEN_WIDTH - PADDLE_OFFSET - PADDLE_WIDTH, 
        (SCREEN_HEIGHT - PADDLE_LENGTH)/ 2, 
        PADDLE_LENGTH)


    scale = 6
    dy = scale
    dx = scale
end    

function update_circle()
    local collide_wall_y = circle["y"] >= SCREEN_HEIGHT or circle["y"] <= 0
    local collide_wall_x = circle["x"] >= SCREEN_WIDTH or circle["x"] <= 0 
    
    local collide_paddle_player = (
                circle["x"] <= player_paddle["x"] + PADDLE_WIDTH 
            and (circle["y"] > player_paddle["y"] and circle["y"] < player_paddle["y"] + PADDLE_LENGTH)
        )
    
    local collide_paddle_opponent = (
        circle["x"] >= opponent_paddle["x"] 
    and (circle["y"] > opponent_paddle["y"] and circle["y"] < opponent_paddle["y"] + PADDLE_LENGTH)
        )

    local collide_paddle = collide_paddle_player or collide_paddle_opponent

    if collide_wall_y then
        dy = -dy
    end

    if collide_wall_x or collide_paddle then
        dx = -dx
    end    

    circle["x"] = circle["x"] + dx
    circle["y"] = circle["y"] + dy
end

function love.update(dt)
    time_elapsed = time_elapsed + dt
    clip_time = 1/60

    if time_elapsed > clip_time then
        second_count = second_count + 1 
        update_circle()
        time_elapsed = 0
    end
end 

function love.draw()
    -- love.graphics.circle( mode, x, y, radius, segments)
    love.graphics.circle("fill", circle["x"], circle["y"], 10, 100)
    love.graphics.print("Hello habin!", 400, 300)
    draw_paddle(player_paddle)
    draw_paddle(opponent_paddle)
end